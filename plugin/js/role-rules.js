// Styles
const errorStyle = 'border: thin solid #ff5d5d;background-color:#ffc8c8;border-radius: 10px; width:480px; min-height: 30px;display:none;padding: 5px;margin-top:5px';

// Helper Functions
const isInteger = (id) => {
  return (typeof id === 'number' && isFinite(id) && id === parseInt(id, 10));
}

const h = (name, props, children) => {
  const parent = document.createElement(name);
  if (props) {
    Object.keys(props).forEach(key => {
      parent.setAttribute(key, props[key]);
    });
  }
  if (children) {
    if (Array.isArray(children)) {
      children.forEach(child => {
        parent.append(child);
      });
    } else {
      parent.append(children);
    }
  }
  return parent;
};

const removeRule = (index) => {
  return e => {
    e.preventDefault();
    e.stopPropagation();
    rules = getCurrentRules();
    rules.splice(index, 1);
    repaint();
  }
}

const showError = (message) => {
  const errorMessageElement = document.getElementById('error-message');
  errorMessageElement.style.display = 'block';
  errorMessageElement.innerText = message;
}

const isInputValid = (inputField) => {
  if(inputField.value) {
    try {
      let statusIds = JSON.parse(`[${inputField.value}]`);
      return Array.isArray(statusIds) && statusIds.every(isInteger);
    } catch (e) {
      return false;
    }
  } else {
    return true;
  }
}

const validateInputFields = () => {
  const ctStatusInputs = document.getElementsByClassName('ctStatus');
  const ctGroupInputs = document.getElementsByClassName('ctGroups');
  return Object.values(ctGroupInputs).every(isInputValid) && Object.values(ctStatusInputs).every(isInputValid);
}

const createNewRoleRule = (rule, index) => {
  const removeButton = h("button", { id: `remove_${index}`, class: 'button button-secondary' }, "-");
  removeButton.addEventListener('click', removeRule(index));
  const ctStatusInput = h("input", {
    class: "ctStatus",
    type: "text",
    value: rule.ctStatus
  });
  return h("tr", { class: "formGroup", id: `formGroup_${index}` }, [
    h("td", { style: "text-align:center" }, index + 1),
    h("td", {}, ctStatusInput),
    h("td", {}, h("input", { class: "ctGroups", type: "text", value: rule.ctGroups })),
    h("td", {}, h("select", { class: "wpRole" }, [
      ...Object.entries(wpRoles).map(([role, roleName]) => {
        const opt = h(
          "option",
          {
            value: role
          },
          roleName
        );
        opt.selected = role === rule.wpRole;
        return opt;
      })
    ])),
    h("td", {}, removeButton),
  ]);
};

const emptyRule = { ctStatus: "", ctGroups: "", wpRole: "" };

const addNewRule = (e) => {
  e.preventDefault();
  e.stopPropagation();
  rules = getCurrentRules();
  rules.push(emptyRule);
  repaint();
};

const repaint = () => {
  const rlz = rules || [emptyRule];
  const errorMessageElement = h('div', { id: 'error-message', style: errorStyle });
  const addButton = h("button", { class: "button" }, "Add rule");
  addButton.addEventListener("click", addNewRule);
  document.getElementById("app").innerHTML = "";
  document.getElementById("app").append(
    h("table", { id: "roles" }, [
      h('thead', { className: 'formGroup' }, [
        h('th', { className: 'header'}, 'Rule#'),
        h('th', { className: 'header'}, 'CT StatusIds'),
        h('th', { className: 'header'}, 'CT GroupIds'),
        h('th', { className: 'header'}, 'WP Role'),
        h('th')
      ] ),
      ...rlz.map((rule, index) => {
        return createNewRoleRule(rule, index);
      })
    ])
  );
  document.getElementById("app").append(addButton);
  document.getElementById('app').append(errorMessageElement);
};

const getCurrentRules = () => {
  const roleElems = document.getElementById("roles");
  const roleRules = Object.values(roleElems.children).reduce((acc, roleElem) => {
      if (roleElem.className === "formGroup") {
        acc.push({
          ctStatus: roleElem.querySelectorAll(".ctStatus")[0].value,
          ctGroups: roleElem.querySelectorAll(".ctGroups")[0].value,
          wpRole: roleElem.querySelectorAll(".wpRole")[0].value
        });
      }
      return acc;
    }, []);
    return roleRules;
}

const prepareDataForSending = (e) => {
  if(validateInputFields()) {
    document.getElementById("roleRules").value = JSON.stringify(getCurrentRules());
  } else {
    showError("Rule format is invalid, please check your statusIds and groupIds.");
    e.preventDefault();
    e.stopPropagation();
  }
};

document.getElementById("submit").addEventListener("click", prepareDataForSending);

repaint();