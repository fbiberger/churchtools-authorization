<?php
/**
 * Plugin Name:       ChurchTools Authentication
 * Plugin URI:        https://gitlab.com/fbiberger/churchtools-authorization
 * Description:       The plugin allows to authenticate against Wordpress with an account from churchtools. You can define a statusId to restrict access even further
 * Version:           {{VERSION}}
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Fabian Biberger
 * Author URI:        https://fbiberger.de
 * License:           Apache Licence 2.0
 * License URI:       https://www.apache.org/licenses/LICENSE-2.0.html
 * Text Domain:       churchtools-authentication-plugin
 * Domain Path:       /languages
 */

global $cta_db_version, $cta_user_table;
$cta_db_version = '1.0.0';
$cta_user_table = "cta_users";

function cta_create_user_table() {
  global $wpdb, $cta_user_table,$cta_db_version;
  $table_name = $wpdb->prefix.$cta_user_table;
  $charset_collate = $wpdb->get_charset_collate();

  $sql = "CREATE TABLE $table_name (
    id varchar(40) NOT NULL,
    wp_user_id integer NOT NULL,
    PRIMARY KEY  (id)
  ) $charset_collate;";

  require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
  dbDelta( $sql );
  add_option('cta_db_version', $cta_db_version);
}

function cta_install() {
  cta_create_user_table();
  flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'cta_install' );
	
function cta_deactivation() {
    flush_rewrite_rules();
}

register_deactivation_hook( __FILE__, 'cta_deactivation' );

function cta_delete_user_table() {
  global $wpdb;
  $table_name = $wpdb->prefix."cta_users";

  $sql = "DROP TABLE IF EXISTS $table_name";
  $wpdb->query($sql);
}

function cta_delete_options() {
  delete_option('cta_db_version');
  delete_option('cta_backend_url');
  delete_option('cta_member_status_id');
  delete_option('cta_member_status_ids');
  delete_option('cta_username_must_be_email');
  delete_option('cta_password_min_length');
}

function cta_deinstallation(){
  cta_delete_options();
  cta_delete_user_table();
}

//Register JavaScript Files

function cta_register_plugin_scripts () {
  wp_enqueue_script("roleRules", plugins_url("/js/role-rules.js", __FILE__), array(), '1.0', true);
}

//Uninstall hook removed - makes it easier to upgrade plugin
//register_uninstall_hook(__FILE__, 'cta_deinstallation');

function cta_run(){
  require_once plugin_dir_path( __FILE__ ) . 'includes/cta-authenticate.php';
  add_filter( 'authenticate', 'cta_authenticate', 20, 3 );

  require_once plugin_dir_path( __FILE__ ) . 'includes/cta-settings.php';
  add_action('admin_menu', 'cta_settings_page');
}

add_action('admin_init', "cta_register_plugin_scripts");

cta_run();