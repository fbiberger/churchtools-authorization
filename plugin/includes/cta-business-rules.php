<?php

require_once(plugin_dir_path( __FILE__ ) . 'cta-helpers.php');

function cta_user_has_correct_status($ct_user) {
  $member_status_ids = json_decode('['.get_option('cta_member_status_ids').']');
  $no_minimal_status_required = empty($member_status_ids) || in_array(0,$member_status_ids);
  $user_has_required_status = property_exists($ct_user, 'statusId')
  && in_array($ct_user->statusId, $member_status_ids);
  return $no_minimal_status_required || $user_has_required_status;
}

function cta_input_username_filter($username, $password) {
  $username_must_be_email = get_option('cta_username_must_be_email');
  $password_min_length = get_option('cta_password_min_length');
  return (empty($username_must_be_email) || filter_var($username, FILTER_VALIDATE_EMAIL))
    && (empty($password_min_length) || strlen($password) >= $password_min_length);
}

function status_is_in_rule($rule_status_ids, $statusId) {
  return count($rule_status_ids) === 0 || in_array($statusId, $rule_status_ids);
}

function group_ids_intersect_with_rule($rule_group_ids, $group_ids) {
  if($rule_group_ids == NULL) {
    return true;
  }
  if($group_ids == NULL) {
    return false;
  }
  return count(array_intersect($rule_group_ids, $group_ids)) > 0;
}

function cta_user_get_role($statusId, $group_ids) {
  $roleRules = json_decode(get_option('cta_roleRules'));
  foreach($roleRules as $roleRule) {
    $rule_status_ids = json_decode('['.$roleRule->ctStatus.']');
    $rule_group_ids = json_decode('['.$roleRule->ctGroups.']');
    if(status_is_in_rule($rule_status_ids, $statusId) && group_ids_intersect_with_rule($rule_group_ids, $group_ids)) {
      return $roleRule->wpRole;
    }
  }
  return null;
}