<?php

require_once(plugin_dir_path( __FILE__ ) . 'cta-helpers.php');

function cta_register_ct_user($ct_user, $wp_user_id) {
  global $wpdb, $cta_user_table;
  $table_name = $wpdb->prefix.$cta_user_table;
  $sql = "INSERT INTO $table_name(id, wp_user_id) VALUES('".$ct_user->guid."',$wp_user_id)";
  return $wpdb->query($sql);
}

function cta_register_user($ct_user) {
  $random_password = wp_generate_password( $length = 40, $include_standard_special_chars = false );
  $user_id = wp_create_user( $ct_user->cmsUserId, $random_password, $ct_user->email);
  if(!is_wp_error($user_id) && cta_register_ct_user($ct_user, $user_id)) {
    return cta_get_wp_user_for_ct_user($ct_user);
  } else {
    error_log("[cta] ChurchTool Account could not be registered at Wordpress");
    return new WP_Error( 'cta_error', __( "Account could not be registered", 'churchtools-authentication-plugin' ));
  }
}
