<?php

function starts_with($haystack, $needle)
{
     $length = strlen($needle);
     $extracted = substr($haystack, 0, $length);
     return ($extracted === $needle);
}

function cta_split_status_ids($str_ids) {
  return json_decode('[' . $string . ']', true);
}

function cta_retrieve_auth_cookie($response) {
  $needle = "ChurchTools_ct_";
  foreach (wp_remote_retrieve_cookies($response) as $cookie) {
    if(substr($cookie->name, 0, strlen($needle)) === $needle) {
      return $cookie;
    }
  }
  return NULL;
}

function cta_get_wp_user_for_ct_user($ct_user) {
  return get_user_by('email', $ct_user->email);
}

function cta_get_user_id($username) {
  $user_id_for_username = username_exists($username);
  $user_id_for_email = email_exists($username);

  if($user_id_for_email) {
    return $user_id_for_email;
  } else {
    if($user_id_for_username) {
      return $user_id_for_username;
    }
  }
  return false;
}

function cta_is_ct_user ($user_id) {
  global $wpdb, $cta_user_table;
  $table_name = $wpdb->prefix.$cta_user_table;
  $count = $wpdb->query("SELECT * FROM $table_name WHERE wp_user_id=$user_id");
  return $count > 0;
}