<?php

function fun_name_prop($item) { return $item['name']; }

function cta_settings_page_html()
{
    // check user capabilities
    if (!current_user_can('manage_options')) {
        return;
    }   
    settings_errors( 'cta_messages' );
    ?>
    <div class="wrap">
        <h1><?= esc_html(get_admin_page_title()); ?></h1>
        <form action="options.php" method="post">
            <?php
            settings_fields('cta_settings');
            do_settings_sections('cta_settings');
            do_settings_sections('cta_rules'); 
            ?>

            <input id="roleRules" name="cta_roleRules" type="hidden" value="" />
            <script>
              var rules, wpRoles;
              try {
                rules = JSON.parse('<?= get_option("cta_roleRules") ?>');
              } catch(e) {
                rules = [];
              }
              try {
                wpRoles = JSON.parse('<?= json_encode(array_map('fun_name_prop',  wp_roles()->roles)) ?>');
              } catch (e) {
                wpRoles = [];
              }
            </script>
            <div id="app"></div>

            <?php
              submit_button('Save Settings');
            ?>
        </form>
    </div>
    <?php
}

function cta_member_status_ids_validation_cb ($input) {
  $clean_input = sanitize_text_field($input);
  $input_array = json_decode('['.$clean_input.']');
  if (empty($clean_input) || $input_array) {
    return $clean_input;
  }
  add_settings_error( 'cta_messages', 'cta_message', __( 'Invalid string for member status ids', 'cta_settings' ) );
  return "";
}

function cta_url_validation_cb($input) {
  $clean_input = sanitize_text_field($input);
  if(filter_var($clean_input, FILTER_VALIDATE_URL)) {
    return $input;
  }
  add_settings_error( 'cta_messages', 'cta_message', __( 'Invalid string for url', 'cta_settings' ) );
  return "";
}

function cta_settings_init() {
  register_setting( 'cta_settings', 'cta_backend_url', 'cta_url_validation_cb');
  register_setting( 'cta_settings', 'cta_member_status_ids', 'cta_member_status_ids_validation_cb' );
  register_setting( 'cta_settings', 'cta_username_must_be_email');
  register_setting( 'cta_settings', 'cta_password_min_length' );
  register_setting( 'cta_settings', 'cta_roleRules' );

  add_settings_section('cta_settings_section',
  'Churchtools Authorization Settings Section',
  'cta_settings_section_cb',
  'cta_settings');

  add_settings_section('cta_settings_section',
  'Churchtools Authorization Rules',
  'cta_settings_section_cb',
  'cta_rules');

  add_settings_field(
    'cta_backend_url_field',
    'Churchtools Base Url',
    'cta_settings_field_cb',
    'cta_settings',
    'cta_settings_section'
  );

  add_settings_field(
    'cta_member_status_ids_field',
    'Churchtools Member Status Id',
    'cta_member_statusId_field_cb',
    'cta_settings',
    'cta_settings_section'
  );

  add_settings_field(
    'cta_username_must_be_email_field',
    'Username must be email',
    'cta_username_must_be_email_cb',
    'cta_settings',
    'cta_settings_section'
  );

  add_settings_field(
    'cta_password_min_length_field',
    'Password minimum length',
    'cta_password_min_length_cb',
    'cta_settings',
    'cta_settings_section'
  );
}

add_action( 'admin_init', 'cta_settings_init' );

function cta_settings_page() {
  $hookname = add_options_page(
      'Churchtools Authorization Settings',
      'CTA Settings',
      'manage_options',
      'cta_settings',
      'cta_settings_page_html'
  );

}

// section content cb
function cta_settings_section_cb()
{
    echo NULL;
}
 
// field content cb
function cta_settings_field_cb() {
    // get the value of the setting we've registered with register_setting()
    $setting = get_option('cta_backend_url');
    // output the field
    ?>
    <input type="text" name="cta_backend_url" value="<?php echo isset( $setting ) ? esc_attr( $setting ) : ''; ?>">
    <?php
}

function cta_member_statusId_field_cb() {
    // get the value of the setting we've registered with register_setting()
    $setting = get_option('cta_member_status_ids');
    // output the field
    ?>
    <p>
    <input type="text" name="cta_member_status_ids" value="<?php echo isset( $setting ) ? esc_attr( $setting ) : ''; ?>"></p>
    <p>
      <ul>
      <li>
        <?php echo __('Add one or more statusIds that should qualify a CT user to be able to login to wordpress') ?>
      </li>
      <li>
        <?php echo __('Use comma as separator. (e.g. 1,2,4)') ?>
      </li>
      <li>
        <?php echo __('For every CT account being able to login, leave field empty', 'churchtools-authentication-plugin') ?>
      </li>
    </p>
    <?php
}

function cta_username_must_be_email_cb() {
  // get the value of the setting we've registered with register_setting()
  $setting = get_option('cta_username_must_be_email');
  // output the field
  ?>
  <input name="cta_username_must_be_email" id="cta_username_must_be_email" type="checkbox" value="1" <?php checked( "1", $setting , true )  ?> />
  <?php
}

function cta_password_min_length_cb() {
  // get the value of the setting we've registered with register_setting()
  $setting = get_option('cta_password_min_length');
  // output the field
  ?>
  <input type="number" min="6" name="cta_password_min_length" value="<?php echo isset( $setting ) ? esc_attr( $setting ) : ''; ?>">
  <?php
}