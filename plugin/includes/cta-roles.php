<?php
require_once(plugin_dir_path( __FILE__ ) . 'cta-helpers.php');

function remove_old_ct_roles($wp_user){
  foreach(wp_roles()->roles as $key => $role) {
    if(starts_with($key, 'cta_')) {
      $wp_user->remove_role($key);
    }
  }
}
