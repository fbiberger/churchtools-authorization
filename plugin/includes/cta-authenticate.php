<?php

require_once(plugin_dir_path( __FILE__ ) . 'cta-requests.php');
require_once(plugin_dir_path( __FILE__ ) . 'cta-helpers.php');
require_once(plugin_dir_path( __FILE__ ) . 'cta-register.php');
require_once(plugin_dir_path( __FILE__ ) . 'cta-roles.php');
require_once(plugin_dir_path( __FILE__ ) . 'cta-business-rules.php');

function cta_get_user ($simple_ct_user, $cookie) {
  $response = cta_do_user_get_request($simple_ct_user, $cookie);
  $status_code = wp_remote_retrieve_response_code($response);
  $response_body = wp_remote_retrieve_body($response);

  if($status_code == 200) {
    $ct_user = json_decode($response_body)->data;
    $ct_user->cookie = $cookie;
    return $ct_user;
  } else {
    error_log("[cta] ChurchTool Account could not be retrieved. Status Code: $status_code. Error was: $response_body");
    return new WP_Error( 'cta_error', __("Login Failed", 'churchtools-authentication-plugin'));
  }
}

function get_user_group_ids ($complete_ct_user) {
  $response = cta_do_groups_get_request($complete_ct_user, $complete_ct_user->cookie);
  $status_code = wp_remote_retrieve_response_code($response);
  $response_body = wp_remote_retrieve_body($response);

  if($status_code == 200) {
    $groups = json_decode($response_body)->data;
    return array_map(function($group) { return intval($group->group->domainIdentifier); }, $groups);
  } else {
    error_log("[cta] ChurchTool Groups could not be retrieved. Status Code: $status_code. Error was: $response_body");
    return new WP_Error( 'cta_error', __("Login Failed", 'churchtools-authentication-plugin'));
  }
}

function cta_authenticate_at_churchtools($username, $password) {
  if(!cta_input_username_filter($username,$password)) {
    error_log("[cta] email and password do not fulfill filter rules");
    return new WP_Error('cta_error', 'Login Failed');
  }
  $response = cta_do_authenticate_request($username, $password);
  $status_code = wp_remote_retrieve_response_code($response);
  $response_body = wp_remote_retrieve_body($response);
  $response_cookie = cta_retrieve_auth_cookie($response);

  if($status_code == 200 && $response_cookie) {
    $simple_ct_user = json_decode($response_body)->data;
    $complete_ct_user = cta_get_user($simple_ct_user, $response_cookie);
    if (cta_user_has_correct_status($complete_ct_user)) {
      return $complete_ct_user;
    } else {
      error_log("[cta] User does not have enough privileges to access Wordpress");
      return new WP_Error('cta_error', __("Login Failed", 'churchtools-authentication-plugin'));
    }
  } else {
    error_log("[cta] External Authentication at ChurchTools failed with status code: $status_code. The eror was: $response_body");
    return new WP_Error( 'cta_error', __("Login Failed", 'churchtools-authentication-plugin'));
  }
}

function enrich_wp_user($wp_user, $complete_ct_user) {
  remove_old_ct_roles($wp_user);
  $ct_user_group_ids = get_user_group_ids($complete_ct_user);
  $ct_role = cta_user_get_role($complete_ct_user->statusId, $ct_user_group_ids);
  $wp_user->add_role(strtolower($ct_role));
  return $wp_user;
}

function cta_authenticate($user, $username, $password) {
  global $cta_base_url;
  if(!empty($username) && !empty($password)) {
    $user_id = cta_get_user_id($username);
    if(!empty($user_id)) {
      if(cta_is_ct_user($user_id)) {
        $ct_user = cta_authenticate_at_churchtools($username, $password);
        if(!is_wp_error($ct_user)) {
          $wp_user = cta_get_wp_user_for_ct_user($ct_user);
          if(!is_wp_error($wp_user)) {
            return enrich_wp_user($wp_user, $ct_user);
          }
        }
      }
      return $user;
    } else {
      $ct_user = cta_authenticate_at_churchtools($username, $password);
      if(!is_wp_error($ct_user)) {
        $wp_user = cta_register_user($ct_user);
        if(!is_wp_error($wp_user)) {
          return enrich_wp_user($wp_user,$ct_user);
        }
      } else {
        error_log('Authentication at Churchtool failed:'.$ct_user->get_error_message());
        return $user;
      }
    }
  }
  return $user;
}