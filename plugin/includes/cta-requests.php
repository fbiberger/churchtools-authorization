<?php

function cta_do_authenticate_request($username, $password) {
  $cta_backend_url = get_option('cta_backend_url');

  $headers = array(
    'Accept' => 'application/json',
    'Content-Type'=>'application/json'
  );

  $body = array(
    'username'=> $username,
    'password'=> $password
  );

  return wp_remote_post($cta_backend_url.'/api/login', array('headers' => $headers, 'body' => wp_json_encode($body)));
}

function cta_do_user_get_request($ct_user, $cookie) {
  $cta_backend_url = get_option('cta_backend_url');
  $headers = array(
    'Accept' => 'application/json',
    'Content-Type'=>'application/json',
    'Cookie'=> $cookie->getHeaderValue()
  );
  return wp_remote_get("$cta_backend_url/api/persons/".$ct_user->personId, array('headers' => $headers));
}

function cta_do_groups_get_request($ct_user, $cookie) {
  $cta_backend_url = get_option('cta_backend_url');
  $headers = array(
    'Accept' => 'application/json',
    'Content-Type'=>'application/json',
    'Cookie'=> $cookie->getHeaderValue()
  );
  return wp_remote_get("$cta_backend_url/api/persons/".$ct_user->id."/groups", array('headers' => $headers));
}