# ChurchTools Authorization

This Wordpress plugin allows users to login to Wordpress with their [ChurchTools](https://www.church.tools) account.

## Features
The plugin adds an admin panel to the settings part of the Admin-Backend of Wordpress ("CTA Settings"). It allows the definition of the url under which the ChurchTools Backend is available. You have to add this URL for the plugin to work.
Furthermore it allows to change the statusId with which a user is authorized to access wordpress.

![Settings Overview](./assets/settings.png)

For more information about the statusId see section Status of the REST-API-Documentation (available for ChurchTools Subscribers under: [https://your-church.church.tools/api#/](https://your-church.church.tools/api#/)).

## Concept
If a user wants to login to Wordpress, the plugin checks, if the account is available as Wordpress user. If it is, the user will be logged in via the default Wordpress mechanism.
If not, it tries to authenticate at the ChurchTools API. In case of the account being available in ChurchTools and not in Wordpress, a user is registered at WP and logged in.
If the user is already available in both systems the account is just logged in.

If the authentication fails the user is not allowed to access the system.

In all positive cases, the user gets the default Wordpress Cookie and is authenticated via that cookie until it expires (default period: 14d). It is possible to change that via plugins, if necessary. As changes in ChurchTools user states are not represented in that time, it makes sense to set the expiration date to the default ChurchTools Cookie expiration time (which is 1d).

### Role-Rules
If you want to adapt your settings even further you can add some rules that link one or more statusIds and one or more groupIds to a Wordpress role. Due to restrictions in the Church Tools API, you need to find the CT Ids to add them to the according lists. Then you can add a Wordpress Role via the dropdown. The first rule in the list will apply.

If you leave one of the fields empty, this part will allways match.

When a user logs in, all roles with a "CTA_" prefix will be removed and re-added based on the settings in Churchtools. All other roles will stay present, even though you might not be able to change them in the default Wordpress-UI.

## Uninstall
The plugin does not erase any data on uninstall. The main reason for this behaviour is the ease of upgrades via ZIP-Archive. If you want to remove the data of this plugin entirely, remove the table {wp_table_prefix}_cta_users and the options:

- cta_db_version
- cta_backend_url
- cta_member_status_id
- cta_member_status_ids
- cta_username_must_be_email
- cta_password_min_length
- cta_roleRules

## Licence
Copyright 2020, Fabian Biberger IT-Dienstleistungen

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
