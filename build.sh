#!/bin/bash
rm -r dist
mkdir -p dist/churchtools-authentication
cp readme.md dist/churchtools-authentication
cp -R plugin/* dist/churchtools-authentication
export VERSION=$1
echo $VERSION
sed -i s/{{VERSION}}/$VERSION/g dist/churchtools-authentication/churchtools-authentication.php
cd dist
echo "Created new file dist/cta_$VERSION.zip"
zip -r cta_$VERSION.zip ./churchtools-authentication